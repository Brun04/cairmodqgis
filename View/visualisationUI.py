# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CairModQGIS Viewer Ui
                                 
 Form implementation generated from reading ui file 'visualisation.ui'
 Created by: PyQt5 UI code generator 5.12.3
 
                             -------------------
        Created on 24/03/2020
        Author: C. Bellon, E. Corvi, L. Martelet, B. Verchère
        Copyright: (C) 2020 by InVisu

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_View(object):
    def setupUi(self, View):
        View.setObjectName("View")
        View.resize(400, 313)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/plugins/Cair_Mod_QGIS/ressources/logo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        View.setWindowIcon(icon)
        View.setStyleSheet("border-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(180, 157, 90, 255), stop:1 rgba(236, 219, 171, 255));\n"
"font: 63 13pt \"Spectral SemiBold\";\n"
"background-color: rgb(189, 180, 170);")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(View)
        self.verticalLayout_2.setContentsMargins(-1, 3, 3, 7)
        self.verticalLayout_2.setSpacing(3)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.frame_7 = QtWidgets.QFrame(View)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame_7.sizePolicy().hasHeightForWidth())
        self.frame_7.setSizePolicy(sizePolicy)
        self.frame_7.setMinimumSize(QtCore.QSize(0, 40))
        self.frame_7.setStyleSheet("image: url(:/plugins/Cair_Mod_QGIS/ressources/logo_cairmod_qgis.png);")
        self.frame_7.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_7.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_7.setObjectName("frame_7")
        self.verticalLayout_2.addWidget(self.frame_7)
        self.zone_scroll = QtWidgets.QScrollArea(View)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.zone_scroll.sizePolicy().hasHeightForWidth())
        self.zone_scroll.setSizePolicy(sizePolicy)
        self.zone_scroll.setStyleSheet("background-color: rgb(249, 243, 228);\n"
"font: 63 11pt \"Spectral SemiBold\";")
        self.zone_scroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.zone_scroll.setWidgetResizable(True)
        self.zone_scroll.setAlignment(QtCore.Qt.AlignCenter)
        self.zone_scroll.setObjectName("zone_scroll")
        self.widget_scroll = QtWidgets.QWidget()
        self.widget_scroll.setGeometry(QtCore.QRect(0, 0, 386, 258))
        self.widget_scroll.setObjectName("widget_scroll")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.widget_scroll)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.vbox = QtWidgets.QVBoxLayout()
        self.vbox.setObjectName("vbox")
        self.verticalLayout_4.addLayout(self.vbox)
        self.zone_scroll.setWidget(self.widget_scroll)
        self.verticalLayout_2.addWidget(self.zone_scroll)

        self.retranslateUi(View)
        QtCore.QMetaObject.connectSlotsByName(View)

    def retranslateUi(self, View):
        _translate = QtCore.QCoreApplication.translate
        View.setWindowTitle(_translate("View", "Visualisation"))
from ..resources import *
