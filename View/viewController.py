# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CairModQGIS View Controller
                             -------------------
        Created on 24/03/2020
        Author: C. Bellon, E. Corvi, L. Martelet, B. Verchère
        Copyright: (C) 2020 by InVisu

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 """

import os
from string import *
from qgis.PyQt.QtCore import Qt
from qgis.PyQt import QtWidgets
from qgis.PyQt.QtGui import QPixmap
from qgis.core import QgsGeometry
from qgis.core import QgsPointXY
from qgis.core import QgsCoordinateTransform
from qgis.core import QgsCoordinateReferenceSystem
from qgis.core import QgsProject
from qgis.utils import iface
from .pictureResult import PictureResult
from .viewInterface import ViewInterface

TR1 = QgsCoordinateTransform(
    QgsCoordinateReferenceSystem(4326),
    QgsCoordinateReferenceSystem(3857),
    QgsProject.instance()
)


class ViewController():
    """
    Viewer controller.

    Args:
        plugin (CairModQGIS plugin instance):  plugin
    Attributes:
        plg (CairModQGIS instance): plugin
        linksBnF (list): links to Gallica
        picturesFiles (list): string paths to the thumbnails
        selectedNames (list): pictures names

    """

    def __init__(self, plugin):
        """Initialise and show the viewer."""
        self.plg = plugin
        self.linksBnF = []
        self.pictureFiles = []
        self.selectedNames = []
        self.athar = "https://athar.persee.fr/authority/athar/"
        self.places = {}
        self.logoAthar = os.path.join(
            self.plg.plugin_dir,
            "Ressources",
            "logo_athar.png"
        )

    def set(self):
        """ Set the list of images."""
        nbplaces = 0
        for k in range(len(self.linksBnF)):
            # Place title
            if nbplaces == 0:
                self.setAtharLink(k)
                nbplaces += 1
            if nbplaces == self.places[self.selectedNames[k]]['nbpic']:
                nbplaces = 0
            else:
                nbplaces += 1

            # Box contener of the PICTURE
            temp = QtWidgets.QLabel(self.plg.dlg)
            temp.setAlignment(Qt.AlignCenter)
            temp.setObjectName(self.selectedNames[k] + "img%s" % k)
            pixmap = QPixmap(self.pictureFiles[k])
            if 'full' in self.linksBnF[k] and 'bnf' in self.linksBnF[k]:
                pixmap = pixmap.scaledToWidth(350)
            temp.setPixmap(pixmap)
            self.plg.dlg.vbox.addWidget(temp)

            href = "<p><a href=\"{}\">{}. {}</a></p>" \
                .format(self.linksBnF[k], k+1, self.selectedNames[k])

            # Box contener of the HYPERLINK
            temp = QtWidgets.QLabel(self.plg.dlg)
            temp.setOpenExternalLinks(True)
            temp.setAlignment(Qt.AlignCenter)
            temp.setObjectName(self.selectedNames[k] + "legend%s" % k)
            temp.setText(href)
            self.plg.dlg.vbox.addWidget(temp)

    def setAtharLink(self, k):
        """Set a link to Athar in the viewer."""
        img = QtWidgets.QLabel(self.plg.dlg)
        pixmap = QPixmap(self.logoAthar).scaledToHeight(50)
        img.setPixmap(pixmap)
        img.setStyleSheet("background-color:rgb(214,206,192)")
        url = self.athar+str(self.places[self.selectedNames[k]]['id'])+"/"
        href = "<a href=\"{}\">{}</a>" \
            .format(url, self.selectedNames[k].upper())
        temp = QtWidgets.QLabel(self.plg.dlg)
        temp.setOpenExternalLinks(True)
        temp.setText(href)
        temp.setParent(img)
        temp.setGeometry(50, 0, 350, 50)
        self.plg.dlg.vbox.addWidget(img)

    def clicker(self, point, button):
        """
        Catch an event on QGIS canvas and find the images matching with this point.

        Args:
            point (QEvent): point which is clicked on by the mouse
            button (QButton): informs which mouse button was clicked
        """
        ftIter = None
        try:
            ftIter = iface.activeLayer().getFeatures()
        except AttributeError:
            iface.messageBar().pushMessage(
                "CairModQGIS",
                "Couche Raster selectionnée",
                level=2
            )
        # If the iterator is not valid or the user clicked on the right button
        if ftIter is None or button == 2:
            return

        x, y = point.x(), point.y()
        geom = QgsGeometry().fromPointXY(TR1.transform(QgsPointXY(x, y)))
        rd = self.plg.iface.mapCanvas().scale() / 200
        
        #Resets interface
        if not self.plg.dlg.vbox.isEmpty():
            self.plg.dlg = ViewInterface()
        self.linksBnF = []
        self.pictureFiles = []
        self.selectedNames = []
        self.places = {}
        iface.messageBar().clearWidgets()
        iface.messageBar().pushMessage(
            "CairModQGIS",
            "Récupération des images...",
            level=0
        )

        for ft in ftIter:
            tmp = TR1.transform(ft.geometry().asPoint())
            buffer = QgsGeometry().fromPointXY(tmp).buffer(rd, 26)
            if buffer.contains(geom):
                try:
                    gallica = ft.attribute("note_fr").split(" | ")
                    pics = PictureResult().download_a_place(gallica, ft.attribute("id"))
                    self.pictureFiles += pics
                    self.selectedNames += [ft.attribute("label_fr")]*len(pics)
                    self.linksBnF += gallica
                    self.places[ft.attribute("label_fr")] = {
                        'id':ft.attribute("id"),
                        'nbpic':len(pics)
                    }
                except KeyError:
                    iface.messageBar().pushMessage(
                        "CairModQGIS",
                        "La couche ne contient pas les bons attributs",
                        level=2
                    )
                    return

        iface.messageBar().clearWidgets()
        if len(self.pictureFiles) > 0:
            iface.messageBar().pushMessage(
                "CairModQGIS",
                "%d image(s) trouvée(s)" % len(self.pictureFiles),
                level=3
            )
            self.set()
            self.plg.dlg.show()
        else:
            iface.messageBar().pushMessage(
                "CairModQGIS",
                "Aucune image trouvée. Assurez-vous de cliquer correctement.",
                level=1
            )
