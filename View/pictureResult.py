# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CairModQGIS Picture Result Engine
                             -------------------
        Created on 29/03/2020
        Author: C. Bellon, E. Corvi, L. Martelet, B. Verchère
        Copyright: (C) 2020 by InVisu

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
import urllib
import json
from pathlib import Path
from qgis.utils import iface

class PictureResult():
    """
    This class can donwload images from Gallica.

    Attributes:
        plgPath (str): absolute path to the plugin
        cache (str): absolute path to the plugin cache
        imgFolder (str): images repertory name
    """

    def __init__(self):
        """ Build a PictureResult instance """
        self.plgPath = Path(os.path.dirname(__file__)).parent
        self.cache = os.path.join(self.plgPath, 'cache')
        self.imgFolder = '/img_bnf'

    def reset_cache(self):
        """Reset the images cache."""
        if os.path.exists(self.cache):
            if os.path.exists(self.cache + self.imgFolder):
                files = os.listdir(self.cache + self.imgFolder)
                for fName in files:
                    os.remove(self.cache + self.imgFolder + '/' + fName)

    def set_thumbnail_url(self, picUrl):
        """
        Set the thumbnail url from picUrl.
        
        Attribute:
            picUrl (str):the urls to different images on Gallica, or sometimes on Harvard viewer
        """
        if 'bnf' in picUrl and not 'full/full' in picUrl: #HTML page of Gallica
            #Computing image size for a thumbnail
            ratioValues = picUrl.split('/')[-3].split(',')
            ratio = int(ratioValues[0])/int(ratioValues[1])
            if ratio >= 1:
                thumbRatio = str(int(200*ratio))+',200'
            else:
                thumbRatio = '200,'+str(int(200*1/ratio))
            thumbnailUrl = picUrl.replace(picUrl.split('/')[-3], thumbRatio)
        elif 'harvard' in picUrl: #HTML page of Harvard viewer
            page = urllib.request.urlopen(picUrl).read().decode('utf-8')
            #Retrieve thumbnail URL
            data = page.split('MIRADOR_WOBJECTS: [\n')[1].split('PATH_DATA:')[0].split('],')[0]
            urlId = json.loads(str(data))['canvasID'].split('canvas-')[1].split('.json')[0]
            url = picUrl.replace('iiif', 'ids').replace('manifests', 'ids').replace('view', 'iiif')
            url = url.split('drs:')[0]+urlId+'/full/,150/0/default.jpg'
            thumbnailUrl = url
        else:
            thumbnailUrl = picUrl
        return thumbnailUrl

    def download_a_place(self, linksBnF, atharId):
        """
        Download all pictures matched with a place

        Args:
            linksBnF (list): links to Gallica
            atharId (int): an entity id in Athar
        """
        # Make sure the folders exist
        if not os.path.exists(self.cache):
            os.mkdir(self.cache)
        if not os.path.exists(self.cache + self.imgFolder):
            os.mkdir(self.cache + self.imgFolder)

        paths = list()
        # Get pictures if not in the cache
        for idx in range(len(linksBnF)):
            picUrl = linksBnF[idx]
            picName = 'thumbnail' + str(atharId) + '_' + str(idx+1) + '.jpg'
            picPath = self.cache + self.imgFolder + '/' + picName
            paths.append(picPath)
            if not os.path.exists(picPath):
                thumbnailUrl = self.set_thumbnail_url(picUrl)
                pic = urllib.request.urlopen(thumbnailUrl).read()
                try:
                    with open(picPath, "wb") as file:
                        file.write(pic)
                except IOError:
                    iface.messageBar().pushMessage(
                        "CairModQGIS",
                        "Erreur lors de l'écriture des images."\
                            +" Assurez-vous d'avoir les droits d'écriture",
                        level=2
                    )
        return paths
