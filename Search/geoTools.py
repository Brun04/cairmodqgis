#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CairModQGIS GeoTools
 
 QGIS vector object and georeferenced object management.
                             -------------------
        Created on 27/03/2020
        Author: C. Bellon, E. Corvi, L. Martelet, B. Verchère
        Copyright: (C) 2020 by InVisu

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from qgis.core import (QgsVectorLayer, QgsField, QgsFeature,
                       QgsGeometry, QgsPointXY, QgsMarkerSymbol)
from qgis.PyQt.QtCore import QVariant
from qgis import processing


def createBufferFeature(lat, lng, radius):
    """
    Create a buffer feature.

    Args:
        lat,lng (float): buffer center coordinates
        radius (float): buffer radius
    Returns:
        a circle feature
    """
    pointLayer = QgsVectorLayer("Point", "", "memory")
    pr = pointLayer.dataProvider()
    pr.addAttributes([QgsField("id", QVariant.Int)])
    pointLayer.updateFields()

    fet = QgsFeature()
    fet.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(lat, lng)))
    fet.setAttributes([1])
    pr.addFeatures([fet])
    pointLayer.updateExtents()

    buffer = processing.run("native:buffer", {
        'INPUT': pointLayer,
        'DISTANCE': radius/110,
        'OUTPUT':'memory:'
    })
    return [fet for fet in buffer['OUTPUT'].getFeatures()][0]

def setSymbols(layer):
    """Set symbols for a layer."""
    symbol = {'name': 'triangle', 'color': 'blue', 'size': '3'}
    layer.renderer().setSymbol(QgsMarkerSymbol.createSimple(symbol))
    layer.triggerRepaint()
    return layer

def createPointsLayer(points, layerName="Search results"):
    """
    Create a temporary layer containing points.

    Args:
        points (list): points to transform in a QGIS layer
        layerName (str): layer's name displayed in QGIS layers manager
    Returns:
        a temporary layer
    """
    pointsLayer = QgsVectorLayer("Point", layerName, "memory")
    pr = pointsLayer.dataProvider()
    if len(points) > 0:
        fields = [QgsField(key, QVariant.String) for key in points[0]['properties'].keys()]
        pr.addAttributes(fields)
        pointsLayer.updateFields()
        for point in points:
            fet = QgsFeature()
            fet.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(
                point['geometry']['coordinates'][0],
                point['geometry']['coordinates'][1])))
            fet.setAttributes([point['properties'][key] for key in point['properties'].keys()])
            pr.addFeatures([fet])
        pointsLayer.updateExtents()
    return setSymbols(pointsLayer)

def getDoublets(buildings, criteria):
    """
    Get the results which have the correct number of criteria.

    Args:
        buildings (list) : raw list of results
        criteria (int): number of criteria applied
    Returns:
        the list of buildings
    """
    seen = {}
    for b in buildings:
        if b['properties']['id'] in seen:
            seen[b['properties']['id']]['occ'] += 1
        else:
            seen[b['properties']['id']] = {'occ': 1, 'build':b}
    return [seen[k]['build'] for k in seen if seen[k]['occ'] == criteria]

def createGeomFromPointXY(x, y):
    """Create a geometry from an XY point."""
    return QgsGeometry.fromPointXY(QgsPointXY(x, y))

def intersects(g1, g2):
    """Return true if g1 intersects with g2."""
    return g1.intersects(g2)
