#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CairModQGIS Cursor Slider Item
                             -------------------
        Created on 18/04/2020
        Author: C. Bellon, E. Corvi, L. Martelet, B. Verchère
        Copyright: (C) 2020 by InVisu

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt5.QtCore import QObject
from PyQt5.QtQuick import QQuickItem

class RangeSlider(QObject):
    """Class representing a range slider."""

    def __init__(self, context, parent=None, controller=None):
        """Initialize the cursor."""
        super(RangeSlider, self).__init__(parent)
        self.win = parent
        self.ctx = context
        if parent and controller:
            self.startHandle = parent.children()[0]
            self.endHandle = parent.children()[1]
            self.startHandle.children()[0].xChanged.connect(self.startHandleValueChanged)
            self.endHandle.children()[0].xChanged.connect(self.endHandleValueChanged)
            self.controller = controller

    def startHandleValueChanged(self):
        """Call the controller when the first handle moves."""
        self.controller.plg.dlg.date_min.move(self.startHandle.children()[0].x(), self.controller.plg.dlg.date_min.y())
        return self.controller.dateChanged(0, self.startHandle.property("value"))

    def endHandleValueChanged(self):
        """Call the controller when the second handle moves."""
        self.controller.plg.dlg.date_max.move(self.endHandle.children()[0].x(), self.controller.plg.dlg.date_max.y())
        return self.controller.dateChanged(1, self.endHandle.property("value"))
    