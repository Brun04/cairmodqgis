#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CairModQGIS Search Engine
                             -------------------
        Created on 24/03/2020
        Author: C. Bellon, E. Corvi, L. Martelet, B. Verchère
        Copyright: (C) 2020 by InVisu

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import json
from anytree.importer import JsonImporter
from anytree import findall, PreOrderIter
from .geoTools import createBufferFeature, createGeomFromPointXY, intersects


class SearchEngine:
    """
    Class representing the CairModQGIS search engine.

    Atributes:
        thid (int): thesaurus and gazetteer identifiant
        importer (anytree.importer): thesaurus importer type
        gaz (object): gazetteer
        th (anytree): thesaurus
        path (str): path to the gazetteer
    """

    def __init__(self):
        """ Initialize a search engine."""
        self.importer = JsonImporter()
        self.thid = None
        self.gaz = None
        self.th = None

    def getGazetteer(self, thid, gazPath, thPath):
        """
        Get the gazetteer and the thesaurus from the cache.
        
        Args:
            thid (int): identifiant of the gazetteer and the thesaurus wanted
            gazPath (str): path to the gazetteer
            thPath (str): path to the thesaurus
        Returns:
            1 if no problem is encountered, None otherwise
        """
        self.thid = thid
        self.importer = JsonImporter()
        try:
            with open(gazPath, "r") as gz:
                self.gaz = json.loads(gz.read())
            with open(thPath, "r") as th:
                self.th = self.importer.import_(th.read())
            return 1
        except IOError:
            return None

    def searchByName(self, structName=None):
        """
        Search a building by its name.

        Args:
            structName (str): structure name to search
        Returns:
            list of buildings with structName in their name, or None
        """
        results = []
        if structName and self.thid:
            structName = structName.lower()
            for building in self.gaz['features']:
                if structName in building['properties']['label_fr'].lower():
                    results.append(building)
        return results

    def searchByBuildingType(self, category=None):
        """
        Search buildings by their type.

        Args:
            category (str): type to search
        Returns:
            list of buildings with the input category
        """
        results = []
        if category and self.thid:
            categories = self.searchByLabel(category)
            for building in self.gaz['features']:
                for ctg in building['properties']['isA'].split(';'):
                    if ctg in categories:
                        results.append(building)
        return results

    def searchByLabel(self, category):
        """
        Search in a thesaurus the corresponding id to category.

        Args:
            category (str): building type
        Returns:
            the id corresponding to the category and all underlying id in the thesaurus
        """
        #Root normalization to search in all the tree
        self.th.label = {'label_ala':'', 'label_ar':'', 'label_en':'',
                         'label_fr':'', 'label_iso': '', 'label_mul':''}
        results = findall(self.th, filter_=lambda n: category.lower() in n.label['label_fr'])
        return [b.name for r in results for b in PreOrderIter(r)]

    def getLabels(self, ids):
        """
        Search categories refering to ids.

        Args:
            ids (list): list of category identifiants
        Returns:
            list of the labels corresponding to the id in the thesaurus
        """
        self.th.label = {'label_ala':'', 'label_ar':'', 'label_en':'',
                         'label_fr':'', 'label_iso': '', 'label_mul':''}
        results = []
        for ide in ids:
            results += [b.label['label_fr'] for r in findall(self.th, filter_=lambda n: ide == n.name) for b in PreOrderIter(r) if b.label['label_fr'] not in results]
        return results

    def searchWithBuffer(self, lat, lng, radius):
        """
        Search buildings within a buffer.

        Args:
            lat,lng (float or str): buffer center coordinates
            radius (float or str): buffer radius
        Returns:
            list of buildings within the buffer
        """
        res = []
        if lat != "" and lng != "" and radius != "":
            buffer = createBufferFeature(float(lat), float(lng), float(radius)).geometry()
            for building in self.gaz['features']:
                g = createGeomFromPointXY(
                    float(building['geometry']['coordinates'][0]),
                    float(building['geometry']['coordinates'][1])
                )
                if intersects(g, buffer):
                    res.append(building)
        return res

    def searchWithSelection(self, layer):
        """
        Search buildings selected in layer.

        Args:
            layer (object): gazetteer layer
        Returns:
            list of selected buildings
        """
        res = []
        for f in layer.selectedFeatures():
            for building in self.gaz['features']:
                if f['id'] == building['properties']['id']:
                    res.append(building)
        return res

    def formateDate(self, dateString):
        """
        Transform a string of dates in a sorted list of dates.
        
        Args:
            dateString (str): a string of dates
        Returns:
            list of the dates, sorted
        """
        dates = []
        idx = 0
        while idx < len(dateString):
            try:
                n = int(dateString[idx])
                n = int(dateString[idx:idx+4])
                dates.append(n)
                idx += 4
            except ValueError:
                idx += 1
        return dates

    def intersects(self, inf, sup, dates):
        """
        Seek if the intersection of two existing temporal intervals is not empty.

        Args:
            inf, sup (int): infimum and supremum dates of one interval
            dates (ints): building milestones
        Returns:
            True if intersection or if no milestone. False otherwise.
        """
        if len(dates) > 0:
            bornes = [min(dates), max(dates)]
            if bornes[1] < inf:
                return False
            if bornes[0] > sup:
                return False
            return True
        return False

    def searchByDate(self, inf, sup):
        """
        Search buildings with a date between inf and sup.

        Args:
            inf, sup (int): infimum and supremum dates
        Returns:
            list of buildings
        """
        res = []
        if inf and sup:
            for building in self.gaz['features']:
                if self.intersects(inf, sup, self.formateDate(building['properties']['historyNote_fr'])):
                    res.append(building)
        return res

    def getDateLimits(self):
        """Get extremum dates in the gazetteer."""
        limits = []
        for building in self.gaz['features']:
            limits += self.formateDate(building['properties']['historyNote_fr'])
        return [min(limits), max(limits)]
