# CairmodQGIS

CairModQGIS est un plugin pour QGIS 3, utilisé pour la recherche de bâtiments dans un gazetier suivant différents critères de recherche. Un gazetier est un répertoire de noms de lieux associés avec leur position géographique et d'autres informations annexes.
Les résultats de la recherche sont ensuite placés dans une couche de résultats, avec en attributs les informations contenues dans le gazetier.
Il est aussi possible de visualiser les photos des bâtiments disponibles sur [Gallica](https://gallica.bnf.fr/accueil/fr) directement depuis QGIS.
Ce plugin est un portage sur QGIS 3 d'une version pour QGIS 2.

---

## Configuration

Pour l'installation du plugin et l'utilisation du plugin, il vous faut **QGIS 3** installé avec **OsGeo4W**.

Le plugin a été développé et testé uniquement sous **Windows** et aucun test n'a été fait sous **Mac** ou **Linux**. Le fonctionnement correct du plugin ainsi que son installation ne sont donc pas assurés sur **Mac** et **Linux**. 

## Installation

L'installation du plugin se fait en trois étapes, car il nécessite la bibliothèque Python *Anytree* pour fonctionner.
Il vous faut aussi avoir QGIS 3, installé avec sa version incluant OsGeo disponible sur ce lien : [OSGeo](https://www.qgis.org/fr/site/forusers/download.html) (Ne prenez pas l'installateur indépendant, mais l'installateur réseau)

### Etape 1 : Version de Python
Sous Windows, ouvrir le dossier _OSGeo4W_ présent sur le bureau et y ouvrir l’invite de commandes.

Vérifier que l'invite de commandes est bien celle de QGIS 3. Pour ce faire, taper la commande : 

`python-qgis`

La commande fonctionne lorsqu'elle renvoie la version de Python au format 3.XX, comme l'exemple ci-dessous :
```
Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

Pour sortir de la console Python, saisir `exit()`.

**Si la version de Python est correcte, laisser la console ouverte.**

### Etape 2 : Bibliothèque Anytree

Installer ensuite la bibliothèque avec le gestionnaire de packages pip: 

`python-qgis –m pip install anytree`

Pour vérifier que la bibliothèque a bien été installée, taper la commande : 

`python-qgis –m pip show anytree`

La réponse doit ressembler à celle-ci :

```bash
Name: anytree
Version: 2.8.0
Summary: Powerful and Lightweight Python Tree Data Structure..
Home-page: https://github.com/c0fec0de/anytree
Author: c0fec0de
Author-email: c0fec0de@gmail.com
License: Apache 2.0
Location: c:\osgeo4w64\apps\python37\lib\site-packages
Requires: six
Required-by:
```

Vérifier tout particulièrement la version de anytree (**2.8.0**) et la localisation de l'installation qui doit être semblable à :

`c:\osgeo4w64\apps\python37\lib\site-packages`


### Etape 3 : Plugin

Pour l'installation du plugin, vous avez deux possibilités :

##### Installation depuis le .zip

1. Télécharger le [fichier zip](https://gitlab.com/Brun04/cairmodqgis/-/archive/v0.2/cairmodqgis-v0.2.zip)

2. Lancer QGIS 3

3. Aller dans le gestionnaire d'extension de QGIS et aller dans la partie installer depuis un zip

4. Chercher le fichier cairmodqgis.zip puis cliquer sur installer le plugin

##### Installation depuis le dépot sur OsGeo

1. Lancer QGIS 3

2. Aller dans le gestionnaire d'extension de QGIS et aller dans la partie **Non installées**.

3. Chercher le plugin CairModQGIS et cliquer sur **Installer le plugin**.

---

## Utilisation

Le plugin CairModQGIS est découpé en deux outils : 

* Un outil de recherche qui permet de chercher un ou des bâtiments dans un gazetier sélectionné suivant différents critères de recherche.

* Un outil de visualisation qui permet de visualiser les résultats de la recherche en affichant entre autres les images de Gallica liées.

Les deux boutons permettant de lancer ces outils sont présents dans la barre d'outils de QGIS.

### Outil de recherche
![](/Ressources/logo_recherche_mini.png)

L'utilisation de l'outil de recherche se découpe en deux tâches principales : la sélection du gazetier et les critères de recherche. Noter que pour cet outil, des info-bulles sont disponibles pour vous guide dans votre utilisation

#### Sélection du gazetier

Le menu déroulant contient toutes les propositions de gazetier disponibles. Une fois sélectionné, le gazetier sera chargé et présent sous forme d'une couche dans votre projet QGIS. Vous pouvez alors commencer la recherche.

#### Critères de recherche

Différents critères de recherche sont disponibles et certains sont utilisables conjointement.

Les différents critères sont : 

- Le nom du bâtiment. Une fois que vous avez commencé à entrer un nom, des propositions de bâtiments présents dans le gazetier sont données. Ce critère est combinable avec le critère de date

- Le type du bâtiment. Une fois que vous avez commencé à entrer un type, des propositions de types présents dans le gazetier sont données. Ce critère est combinable avec le critère de date et le critère de position géographique.

- La position géographique. Ce critère peut être utilisé soit avec une sélection QGIS préalable des éléments dans la couche du gazetier, soit avec un buffer d'une taille donnée autour d'un point dont la position est donnée. Ce critère est combinable avec le critère de date et de type.

- La date. Ce critère permet de sélectionner un intervalle temporel sur lequel la recherche va se faire. Ce critère est combinable avec tous les autres.

Une fois que vous êtes satisfait de votre recherche, vous pouvez la valider et une couche sera créée avec les résultats de la recherche.

### Outil de visualisation
![](/Ressources/logo_visualisation_mini.png)

L'outil de visualisation permet d'avoir un aperçu des images liées aux bâtiments trouvés à la suite d'une recherche. Pour l'utiliser, cliquer sur le bouton puis sur un élément de la couche de résultat.
Une fenêtre va ensuite s'ouvrir avec les images liées aux bâtiments proches du clic et avec un lien vers les mêmes images en haute définition.

Pour plus de détails, consulter la documentation disponible [ici](https://gitlab.com/Brun04/cairmodqgis/-/raw/master/Documentation/Documentation_utilisateurs.pdf?inline=false)

---

## Auteurs

© Julie Erismann, Bulle Tuil Leonetti, Laboratoire InVisu.

© Clémence Bellon, Enzo Corvi, Lucas Martelet, Bruno Verchère, ENSG Géomatique.

Le plugin CairModQGIS a été développé à partir du plugin ©CairoData disponible pour QGIS 2 réalisé par Antonin Fondere, Corentin Crapart, Erwan Viegas, Max Permalnaick, Sinda Thaalbi, Tristan Harling et Witold Podlejski.

## Licence

